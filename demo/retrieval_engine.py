import json
from sentence_transformers import SentenceTransformer, util, LoggingHandler, models
import numpy as np
import logging
import torch
from pyserini.search.lucene import LuceneSearcher
import faiss
from utils import process_context, combined_score_mm

logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])
logger = logging.getLogger(__name__)


def load_json(path):
    return json.load(open(path, "r", encoding="utf-8"))


def load_test_data(test_path):
    data = load_json(test_path)
    questions = []
    answers = []
    for qa in data:
        questions.append(qa['question'])
        answers.append(qa['answer_id'])
        
    return questions, answers


def load_corpus(corpus_path):
    corpus = []
    data = load_json(corpus_path)
    for psg in data:
        if isinstance(psg, str):
            corpus.append(psg)
        else:
            text = psg['text']
            if psg['title'] != '':
                text = psg['title'] + ' . ' + text
            corpus.append(text)

    return corpus

class Retriever:

    def __init__(self) -> None:
        self.encoder_path='../evaluation/dual_checkpoint'
        self.save_path='../evaluation/embeddings/legal_embeddings.npy'
        self.device='cuda'
        self.corpus_path='../evaluation/eval_data/legal_corpus.json'
        self.eval_data_path='../evaluation/eval_data/zalo_legal_queries.json'
        self.create_new=False
        self.sparse_index_path = '../evaluation/indexes_bm25/legal_index'
        self.dense_index_path = '../evaluation/indexes_dense/legal_index/index_hnsw.index'
        self.sparse_searcher = LuceneSearcher(self.sparse_index_path)
        self.sparse_searcher.set_bm25(k1=0.5, b=0.5)
        self.ENCODER = SentenceTransformer(self.encoder_path).to(self.device)
        self.dense_searcher = faiss.read_index(self.dense_index_path)
        self.corpus_embeddings = torch.from_numpy(np.load(self.save_path))
            
        self.corpus_embeddings = self.corpus_embeddings.to(self.device)
        self.CORPUS = load_corpus(self.corpus_path)
    
    def retrieve(self, query, top_k = 5):
        bm25_hits = self.sparse_searcher.search(process_context(query), k=top_k)
        sparse_result = {hit.docid: hit.score for hit in bm25_hits}
        question_embedding = self.ENCODER.encode(query, show_progress_bar=False)
        question_embedding = question_embedding / np.linalg.norm(question_embedding)
        question_embedding = np.expand_dims(question_embedding, axis=0)
        scores, corpus_ids = self.dense_searcher.search(question_embedding, k=top_k)
        dense_result = {str(corpus_id): score for corpus_id, score in zip(corpus_ids[0], scores[0])}
        hybrid_result = combined_score_mm(sparse_result, dense_result, top_k=top_k)
        return [self.CORPUS[int(res[0])] for res in hybrid_result]
              
    
