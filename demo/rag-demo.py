import streamlit as st
from openai import OpenAI
from retrieval_engine import Retriever

def zero_shot_QA_prompt(query, contexts):
    prompt =('Nhiệm vụ của bạn là tìm ra câu trả lời chính xác cho Query từ các contexts được cho như sau:\n'
    + 'Query: ' + query
    + '\n'.join([f'Context {idx+1}: {context}' for idx, context in enumerate(contexts)])
    )
    return prompt

retriever = Retriever()
related_docs =[]
top_k = 5
# Set OpenAI API key from Streamlit secrets
client = OpenAI(api_key=st.secrets["OPENAI_API_KEY"])
st.set_page_config(layout ='wide')
st.title("Legal Text Retrieval")
col1, col2, col3 = st.columns([1, 3, 4],gap='large')

with col1:
    st.subheader("Parameters")
    top_k = st.slider(
        "Top k:",
        min_value = 5,
        max_value = 100,
        step = 1)
    
with col2:
    
    # Set a default model
    if "openai_model" not in st.session_state:
        st.session_state["openai_model"] = "gpt-3.5-turbo"

    # Initialize chat history
    if "messages" not in st.session_state:
        st.session_state.messages = []

    # Display chat messages from history on app rerun
    # for message in st.session_state.messages:
    #     with st.chat_message(message["role"]):
    #         st.markdown(message["content"])

    # Accept user input
    if prompt := st.chat_input("What is up?"):
        # st.session_state.messages = []
        # Add user message to chat history
        st.session_state.messages.append({"role": "user", "content": prompt})
        # Display user message in chat message container
        with st.chat_message("user"):
            st.markdown(prompt)
        related_docs = retriever.retrieve(prompt, top_k)
        # Display assistant response in chat message container
        with st.chat_message("assistant"):
            m = st.session_state.messages[-1]
            stream = client.chat.completions.create(
                model=st.session_state["openai_model"],
                
                messages=[
                    {"role": m["role"], "content": zero_shot_QA_prompt(m["content"], related_docs)}
                ],
                stream=True,
            )
            response = st.write_stream(stream)
with col3:
    st.subheader("Retrieved Passages",)
    if len(related_docs) != 0:
        st.write(related_docs)
    # st.session_state.messages.append({"role": "assistant", "content": zero_shot_QA_prompt(st.session_state.messages[-1]["content"], related_docs)})