python training_dual_mnr_loss.py \
  --train_data_path train_data/all_train.json \
  --dev_data_path train_data/all_dev.json \
  --model_name_or_path evaluation/retro_phobert \
  --batch_size 4 \
  --save_path checkpoint
