# Training Dual-Encoder with sentence-transformers
This repo for training dual-encoder with [MultipleNegativesRankingLoss](https://www.sbert.net/docs/package_reference/losses.html#multiplenegativesrankingloss). You just need question-answer pairs for training, negatives come from in-batch when training.
## Installation
Install with conda:
```
conda env create -f conda_torch_gpu_env.yml
```
## Training
```
sh run_training.sh
```
## Evaluation
### Move to evaluation folder
1. For Dual-Encoder evaluation run:
    ```
    python evaluate_dual.py
    ```
2. For BM25 evaluation run:
   ```
   python convert_to_pyserini_corpus.py
   sh index_bm25.sh
   python evaluate_bm25.py
   ```
3. For hybrid evaluation run:
   ```
   python index_dense.py
   python evaluate_hybrid.py
   ```
