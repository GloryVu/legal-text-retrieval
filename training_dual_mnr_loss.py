import math
from sentence_transformers import models, losses, datasets
from sentence_transformers import LoggingHandler, SentenceTransformer, InputExample
from sentence_transformers.evaluation import BinaryClassificationEvaluator
import logging
import os
import json
import argparse
import random

logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])
logger = logging.getLogger(__name__)

random.seed(12345)


def combined_text(passage):
    text = passage['passage_content']
    if text == '':
        if passage['passage_title'] == '':
            raise "Pair must have at least title or text !!!"
        else:
            text = passage['passage_title']
    else:
        if passage['passage_title'] != '':
            text = passage["passage_title"] + " . " + passage["passage_content"]
    return text


def load_train_set(file_path: str):
    with open(file_path, 'r') as fin:
        data = json.load(fin)
        samples = []
        for qa in data:
            pos_text = combined_text(qa["context"])
            for question in qa["questions"]:
                samples.append(InputExample(texts=[question, pos_text], label=1))
    logger.info(f"There are {len(samples)} pairs for training.")
    logger.info(f"Example for training input: {samples[5]}")
    return samples


def load_dev_set(file_path: str, num_neg: int = 10):
    with open(file_path, 'r') as fin:
        data = json.load(fin)
    samples = []
    for qa in data:
        question = qa['question']
        pos_text = combined_text(qa["context"])
        samples.append(InputExample(texts=[question, pos_text], label=1))
        for neg in qa['negatives'][:num_neg]:
            neg_text = combined_text(neg)
            samples.append(InputExample(texts=[question, neg_text], label=0))
    logger.info(f"There are {len(samples)} pairs for evaluation.")
    logger.info(f"Example for evaluation input: {samples[5]}")
    return samples


def main(args):
    logger.info("Loading and processing data ...!")
    train_samples = load_train_set(args.train_data_path)
    dev_samples = load_dev_set(args.dev_data_path, num_neg=args.num_neg)

    random.shuffle(dev_samples)
    # dev_samples = dev_samples[:2000]

    logger.info("Creating model...!")
    word_embedding_model = models.Transformer(args.model_name_or_path, max_seq_length=args.max_length)
    pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension(), pooling_mode='cls')
    model = SentenceTransformer(modules=[word_embedding_model, pooling_model])

    train_batch_size = args.batch_size
    num_epochs = args.num_epochs
    save_path = os.path.join(args.save_path, 'dual_' + args.model_name_or_path)

    os.makedirs(save_path, exist_ok=True)

    train_dataloader = datasets.NoDuplicatesDataLoader(train_samples, batch_size=train_batch_size)
    train_loss = losses.MultipleNegativesRankingLoss(model=model)

    evaluator = BinaryClassificationEvaluator.from_input_examples(dev_samples, name='dev')

    warmup_steps = math.ceil(len(train_dataloader) * num_epochs * 0.1)  # 10% of train data for warm-up
    evaluation_steps = args.eval_steps
    learning_rate = args.learning_rate
    logger.info("Warmup-steps: {}".format(warmup_steps))
    logger.info("Evaluation-steps: {}".format(evaluation_steps))

    # Train the model
    model.fit(train_objectives=[(train_dataloader, train_loss)],
              evaluator=evaluator,
              epochs=num_epochs,
              evaluation_steps=evaluation_steps,
              warmup_steps=warmup_steps,
              optimizer_params={'lr': learning_rate},
              output_path=save_path,
              use_amp=True)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--train_data_path', default='train_data/all_train.json', type=str)
    parser.add_argument('--dev_data_path', default='train_data/all_dev.json', type=str)
    parser.add_argument('--save_path', default='checkpoint', type=str)
    parser.add_argument('--model_name_or_path', default='evaluation/retro_phobert', type=str)
    parser.add_argument('--learning_rate', default=2e-5, type=float)
    parser.add_argument('--eval_steps', default=1000, type=int)
    parser.add_argument('--max_length', default=256, type=int)
    parser.add_argument('--batch_size', default=128, type=int)
    parser.add_argument('--num_neg', default=5, type=int)
    parser.add_argument('--num_epochs', default=4, type=int)

    args = parser.parse_args()
    logger.info(f"Arguments for training: {args}")
    main(args)
