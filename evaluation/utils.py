import re
import string
import datetime
from typing import Dict


def strip_context(text):
    text = text.replace('\n', ' ')
    text = re.sub(r'\s+', ' ', text)
    text = text.strip()
    return text


def process_context(text, lower=True, remove_=False, remove_punct=True):
    if lower:
        text = text.lower()
    if remove_:
        text = text.replace('_', ' ')
    if remove_punct:
        text = ' '.join(list(filter(remove_punctuation, text.split())))
    return strip_context(text)


def remove_punctuation(w):
    return w not in string.punctuation


def print_message(*s, condition=True):
    s = ' '.join([str(x) for x in s])
    msg = "[{}] {}".format(datetime.datetime.now().strftime("%b %d, %H:%M:%S"), s)

    if condition:
        print(msg, flush=True)

    return msg


def combined_score_mm(sparse_result: Dict, dense_result: Dict, top_k: int = 100, alpha=0.6):
    """Combined score between BM25 and Dense, this function was inspired by pyserini"""
    hybrid_result = {}
    min_dense_score = min(dense_result.values()) if len(dense_result) > 0 else 0
    max_dense_score = max(dense_result.values()) if len(dense_result) > 0 else 1
    min_sparse_score = min(sparse_result.values()) if len(sparse_result) > 0 else 0
    max_sparse_score = max(sparse_result.values()) if len(sparse_result) > 0 else 1
    for psg in set(dense_result.keys()) | set(sparse_result.keys()):
        if psg not in dense_result:
            sparse_score = sparse_result[psg]
            dense_score = min_dense_score
        elif psg not in sparse_result:
            sparse_score = min_sparse_score
            dense_score = dense_result[psg]
        else:
            sparse_score = sparse_result[psg]
            dense_score = dense_result[psg]
        sparse_score = (sparse_score - min_sparse_score) / (max_sparse_score - min_sparse_score)
        dense_score = (dense_score - min_dense_score) / (max_dense_score - min_dense_score)
        score = alpha * dense_score + (1 - alpha) * sparse_score
        hybrid_result[psg] = (score, sparse_score, dense_score)
    return sorted(hybrid_result.items(), key=lambda x: x[1][0], reverse=True)[:top_k]
