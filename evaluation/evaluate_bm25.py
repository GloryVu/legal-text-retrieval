from pyserini.search.lucene import LuceneSearcher
from tqdm.autonotebook import tqdm
from utils import process_context, print_message
import argparse
import numpy as np
import time
import json


def evaluate(args):
    print_message("Load Searcher !!!")
    searcher = LuceneSearcher(args.bm25_path)
    searcher.set_bm25(k1=0.5, b=0.5)
    print_message("Load queries !!!")
    queries = []
    answer_ids = []

    with open(args.eval_data_path, "r", encoding="utf-8") as file:
        test_data = json.load(file)

    for qa in test_data:
        queries.append(process_context(qa['question'], remove_=False, remove_punct=True))
        answer_ids.append(qa['answer_id'])

    print_message("Example for query: ", queries[:5])

    assert len(answer_ids) == len(test_data)

    assert len(answer_ids) == len(queries)

    print_message("Start evaluate !!!")
    counts = np.zeros((6, ))
    start = time.time()
    for query, answer_id in tqdm(zip(queries, answer_ids), total=len(queries)):
        hits = searcher.search(query, k=args.top_k)
        for idx, hit in enumerate(hits):
            if int(hit.docid) == int(answer_id):
                if idx < 1:
                    counts += 1
                elif idx < 5:
                    counts[1:] += 1
                elif idx < 10:
                    counts[2:] += 1
                elif idx < 20:
                    counts[3:] += 1
                elif idx < 50:
                    counts[4:] += 1
                else:
                    counts[5:] += 1
                break

    total_time = time.time() - start
    print_message("Average Time for 1 query: ", total_time / len(queries))
    print_message("Dataset: ", args.eval_data_path)
    print_message(f"Recall@1: {counts[0] / len(queries) * 100}")
    print_message(f"Recall@5: {counts[1] / len(queries) * 100}")
    print_message(f"Recall@10: {counts[2] / len(queries) * 100}")
    print_message(f"Recall@20: {counts[3] / len(queries) * 100}")
    print_message(f"Recall@50: {counts[4] / len(queries) * 100}")
    print_message(f"Recall@100: {counts[5] / len(queries) * 100}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--bm25_path', default='./indexes_bm25/legal_index', type=str)
    parser.add_argument('--eval_data_path', default='./eval_data/zalo_legal_queries.json', type=str)
    parser.add_argument('--top_k', default=100, type=int)

    args = parser.parse_args()
    print_message(f"Arguments for training: {args}")
    evaluate(args)
