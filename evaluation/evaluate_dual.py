import json
from sentence_transformers import SentenceTransformer, util, LoggingHandler, models
import numpy as np
import logging
import time
import argparse


logging.basicConfig(format='%(asctime)s - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.INFO,
                    handlers=[LoggingHandler()])
logger = logging.getLogger(__name__)


def load_json(path):
    return json.load(open(path, "r", encoding="utf-8"))


def load_test_data(test_path):
    data = load_json(test_path)
    questions = []
    answers = []
    for qa in data:
        questions.append(qa['question'])
        answers.append(qa['answer_id'])
        
    return questions, answers


def load_corpus(corpus_path):
    corpus = []
    data = load_json(corpus_path)
    for psg in data:
        if isinstance(psg, str):
            corpus.append(psg)
        else:
            text = psg['text']
            if psg['title'] != '':
                text = psg['title'] + ' . ' + text
            corpus.append(text)

    return corpus


def evaluate(args):
    if args.create_new:
        logger.info(f'Creating Dual-Encoder from {args.encoder_path}...!')
        word_embedding_model = models.Transformer(args.encoder_path, max_seq_length=256)
        pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension(), pooling_mode='cls')
        ENCODER = SentenceTransformer(modules=[word_embedding_model, pooling_model], device=args.device)
    else:
        logger.info(f'Loading Dual-Encoder from {args.encoder_path}...!')
        ENCODER = SentenceTransformer(args.encoder_path).to(args.device)

    logger.info(f'Loading & Encoding corpus from {args.corpus_path}...!')
    CORPUS = load_corpus(args.corpus_path)
    corpus_embeddings = ENCODER.encode(CORPUS, convert_to_tensor=True, batch_size=128)
    logger.info('Save embedding...!')
    np_arr = corpus_embeddings.cpu().detach().numpy()
    with open(args.save_path, 'wb') as f:
        np.save(f, np_arr)
        
    corpus_embeddings = corpus_embeddings.to(args.device)
    start = time.time()
    logger.info(f'Loading & Encoding test data from {args.eval_data_path}...!')
    questions, answers = load_test_data(args.eval_data_path)
    question_embeddings = ENCODER.encode(questions, convert_to_tensor=True)
    question_embeddings = question_embeddings.to(args.device)
    
    logger.info('Searching...!')
    hits = util.semantic_search(question_embeddings, corpus_embeddings, top_k=args.top_k)
    
    logger.info("Computing metric...!")
    counts = np.zeros((6, ))
    for hit, actual_answer in zip(hits, answers):
        for idx, candidate in enumerate(hit):
            corpus_id = candidate["corpus_id"]
            if int(corpus_id) == int(actual_answer):
                if idx < 1:
                    counts += 1
                elif idx < 5:
                    counts[1:] += 1
                elif idx < 10:
                    counts[2:] += 1
                elif idx < 20:
                    counts[3:] += 1
                elif idx < 50:
                    counts[4:] += 1
                else:
                    counts[5:] += 1

    total_time = time.time() - start
    logger.info(f"Dataset: {args.eval_data_path}")
    logger.info(f"Average Time for 1 query: {total_time / len(questions)}")
    logger.info(f"Recall@1: {counts[0] / len(questions) * 100}")
    logger.info(f"Recall@5: {counts[1] / len(questions) * 100}")
    logger.info(f"Recall@10: {counts[2] / len(questions) * 100}")
    logger.info(f"Recall@20: {counts[3] / len(questions) * 100}")
    logger.info(f"Recall@50: {counts[4] / len(questions) * 100}")
    logger.info(f"Recall@100: {counts[5] / len(questions) * 100}")
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--encoder_path', default='./dual_checkpoint', type=str)
    parser.add_argument('--save_path', default='./embeddings/legal_embeddings.npy', type=str)
    parser.add_argument('--device', default='cuda', type=str)
    parser.add_argument('--corpus_path', default='./eval_data/legal_corpus.json', type=str)
    parser.add_argument('--eval_data_path', default='./eval_data/zalo_legal_queries.json', type=str)
    parser.add_argument('--create_new', default=False, type=bool, help='if create dual-encoder from a encoder set True')
    parser.add_argument('--top_k', default=100, type=int)

    args = parser.parse_args()
    evaluate(args)
