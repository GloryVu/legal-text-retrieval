from tqdm.autonotebook import tqdm
import faiss
import argparse
from sentence_transformers import SentenceTransformer
from pyserini.search.lucene import LuceneSearcher
from utils import print_message, process_context, combined_score_mm
import time
import json
import numpy as np


def evaluate(args):
    print_message("Loading Dense Searcher...!")
    faiss.omp_set_num_threads(4)
    dense_searcher = faiss.read_index(args.dense_index_path)
    ENCODER = SentenceTransformer(args.encoder_path).to(args.device)

    print_message('Loading Sparse Searcher...!')
    sparse_searcher = LuceneSearcher(args.sparse_index_path)
    sparse_searcher.set_bm25(k1=0.5, b=0.5)

    print_message('Loading queries...!')
    queries = []
    answer_ids = []
    with open(args.eval_data_path, 'r', encoding="utf-8") as file:
        test_data = json.load(file)

    for qa in test_data:
        queries.append(qa['question'])
        answer_ids.append(qa['answer_id'])

    print_message('Example for query: ', queries[10])

    assert len(answer_ids) == len(test_data)
    assert len(answer_ids) == len(queries)

    print_message('Starting evaluate...!')
    counts = np.zeros((6, ))
    start = time.time()
    for query, answer_id in tqdm(zip(queries, answer_ids), total=len(answer_ids)):
        bm25_hits = sparse_searcher.search(process_context(query), k=args.top_k)
        sparse_result = {hit.docid: hit.score for hit in bm25_hits}
        question_embedding = ENCODER.encode(query, show_progress_bar=False)
        question_embedding = question_embedding / np.linalg.norm(question_embedding)
        question_embedding = np.expand_dims(question_embedding, axis=0)
        scores, corpus_ids = dense_searcher.search(question_embedding, k=args.top_k)
        dense_result = {str(corpus_id): score for corpus_id, score in zip(corpus_ids[0], scores[0])}
        hybrid_result = combined_score_mm(sparse_result, dense_result, top_k=args.top_k)
        results = [int(res[0]) for res in hybrid_result]
        for idx, hit in enumerate(results):
            if int(hit) == int(answer_id):
                if idx < 1:
                    counts += 1
                elif idx < 5:
                    counts[1:] += 1
                elif idx < 10:
                    counts[2:] += 1
                elif idx < 20:
                    counts[3:] += 1
                elif idx < 50:
                    counts[4:] += 1
                else:
                    counts[5:] += 1
                break

    total_time = time.time() - start
    print_message(f"Dataset: {args.eval_data_path}")
    print_message(f"Average Time for 1 query: {total_time / len(queries)}")
    print_message(f"Recall@1: {counts[0] / len(queries) * 100}")
    print_message(f"Recall@5: {counts[1] / len(queries) * 100}")
    print_message(f"Recall@10: {counts[2] / len(queries) * 100}")
    print_message(f"Recall@20: {counts[3] / len(queries) * 100}")
    print_message(f"Recall@50: {counts[4] / len(queries) * 100}")
    print_message(f"Recall@100: {counts[5] / len(queries) * 100}")
    print_message("Done !!!")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('--encoder_path', default='./dual_checkpoint', type=str)
    parser.add_argument('--dense_index_path', default='./indexes_dense/legal_index/index_hnsw.index', type=str)
    parser.add_argument('--sparse_index_path', default='./indexes_bm25/legal_index', type=str)
    parser.add_argument('--device', default='cuda', type=str)
    parser.add_argument('--eval_data_path', default='./eval_data/zalo_legal_queries.json', type=str)
    parser.add_argument('--top_k', default=100, type=int)

    args = parser.parse_args()
    evaluate(args)