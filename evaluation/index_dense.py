import numpy as np
import faiss
import os
import argparse
from utils import print_message


def index_hnsw(index_dense_path: str, embeddings_path: str, embedding_size: int = 768,
               M: int = 32, efC: int = 128, efSearch: int = 256):
    """
    Create a faiss hnsw index
    @param efSearch: efSearch
    @param index_dense_path: path to index
    @param embeddings_path: path to embeddings saved
    @param embedding_size: size of embedding 768, 64, ...
    @param M: number of links per vector
    @param efC: efConstruction
    """
    faiss.omp_set_num_threads(4)
    index = faiss.IndexHNSWFlat(embedding_size, M, faiss.METRIC_INNER_PRODUCT)
    index.hnsw.efConstruction = efC
    index.hnsw.efSearch = efSearch
    corpus_embeddings = np.load(embeddings_path, mmap_mode="r")
    print_message(corpus_embeddings.shape)
    corpus_embeddings = corpus_embeddings / np.linalg.norm(corpus_embeddings, axis=1)[:, None]
    print_message("Adding vectors to index...")
    index.add(corpus_embeddings)
    os.makedirs(index_dense_path, exist_ok=True)
    faiss.write_index(index, os.path.join(index_dense_path, "index_hnsw.index"))

    print_message("Done")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--index_dense_path', default='./indexes_dense/legal_index', type=str)
    parser.add_argument('--embeddings_path', default='./embeddings/legal_embeddings.npy', type=str)
    args = parser.parse_args()

    index_hnsw(args.index_dense_path, args.embeddings_path)
