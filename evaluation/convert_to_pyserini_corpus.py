import json
import os
from tqdm.autonotebook import tqdm
import argparse
from utils import process_context, print_message


def main(args):
    print_message("Load corpus !!!")
    with open(args.corpus_path, 'r', encoding='utf-8') as f:
        corpus = json.load(f)
    print_message("length corpus: ", len(corpus))

    print_message("Converting !!!")
    idx = 0
    corpus_pyserini = []

    for psg in tqdm(corpus):
        if isinstance(psg, str):
            context = psg
        else:
            context = psg['title'] + " " + psg['text']
        corpus_pyserini.append({"id": idx, "contents": process_context(context, remove_=False, remove_punct=True)})
        idx += 1

    os.makedirs(args.save_path, exist_ok=True)

    with open(args.save_path + "/corpus_pyserini.jsonl", "w") as f:
        for context in corpus_pyserini:
            json.dump(context, f, ensure_ascii=False)
            f.write("\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('--corpus_path', default='./eval_data/legal_corpus.json', type=str)
    parser.add_argument('--save_path', default='./bm25_corpus/legal_corpus', type=str)

    args = parser.parse_args()
    print_message(f"Arguments for training: {args}")
    main(args)
