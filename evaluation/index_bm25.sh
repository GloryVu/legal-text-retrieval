python -m pyserini.index.lucene \
  --collection JsonCollection \
  --input ./bm25_corpus/legal_corpus \
  --index ./indexes_bm25/legal_index \
  --generator DefaultLuceneDocumentGenerator \
  --threads 4 